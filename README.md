# Setup

## How-to

To use this tool, change directory to `./src` and run the script `run.sh`.

## Requirements

* g++ version >=7.5.0
* the `boost` library

## Configuration

* the ODE may be specified in `./src/ode.h`. The corresponding parameters
  **must** be specified in `./src/config.h`.
* the data may be given in `./src/config.h`.
* the simulation can be configured in `./src/config.h`, so as to tweak the
  SMC settings such as the integration step, the ranges to be tested.
* the binary settings, such as the number of threads to be used, can be
  specified in `./src/config.h`.

# About

This repo provides the implementation of our technique regarding Statistical
Model Checking applied to parametric Ordinary Differential Equations systems.
It was written by the VELO team of the Laboratoire des Sciences du Numérique
(LS2N) in Nantes, France.
It may possibly not be updated after the submission of the paper it relates to,
which accounts for the QEST 2022[0] conference.

# Update (25/01/2023)

The paper base on this tool got accepted by the QEST commitee, and is
accessible here[1],[2].
We would like to thank them once more. Further, we have the opportunity to
extend it for the next special issue of TOMACS[3], which is due around Nov.
'23.
The branches `compute_aurelia` and `compute_prey` are not subject to change
from their current state (commits ` 7b94d96b92` and `314f3e69ec`,
respectively), even if the tool were updated.

# Update (14/03/2023)

As part of our submission to TOMACS, we present two new branches
`compute_stability` and `compute_basin`, presenting an implementation of our
method in the context of ODE stability analysis.

# Legal

This repository is licensed under the GPLv3 (see [LICENSE](./LICENSE)).
You don't need to notify us if you use it (though it would be appreciated), but
you do need to provide the source code of the entire project.
More details can be found on the GPLv3 webpage[4].

# Links

[0] https://www.qest.org/qest2022/index.html

[1] https://hal.science/view/index/docid/3824899

[2] https://dx.doi.org/10.1007/978-3-031-16336-4\_5

[3] https://dl.acm.org/journal/tomacs

[4] https://www.gnu.org/licenses/gpl-3.0.en.html
