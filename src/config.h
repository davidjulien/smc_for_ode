/*
 * @author      : David JULIEN (based on Gilles ARDOUREL)
 * @date        : Friday, March 25th
 * @license     : GPLv3
 *
 *
 */

#ifndef H_CONFIG
#define H_CONFIG

#include <vector>
#include <cmath>
#include <boost/array.hpp>


/***************** DATA ***************/

const std::vector<double> data_time_points = {0, 1, 4, 7, 8, 11, 15, 18, 22,
        25, 29, 32, 37, 39, 42};

const std::vector<double> data_0 = {7.78, 6.78, 6.91, 6.00, 6.08, 5.83,
        5.35, 5.17, 5.34, 5.51, 5.48, 5.09, 5.54, 5.52, 5.47};
const std::vector<std::vector<double>> data = { data_0 };

const std::vector<double> data_radiuses_0 = {.49, .71, .79, .86, .93, 1.05,
        1.01, 1.06, 1.05, 1.08, 1.11, 1.12, 1.04, 1.05, 1.05};
const std::vector<std::vector<double>> data_radiuses = { data_radiuses_0 };


/**************** ODE CONFIG ***************/

const int state_size = 1;
const int params_size = 2;

typedef boost::array<double, state_size> state_t;
typedef boost::array<double, params_size> params_t;
typedef boost::array<double, 2> range; // min, max
typedef boost::array<range, params_size> params_ranges_t;
typedef std::tuple<params_t, float, float> simres_t;


const double dt = .1;
const double t_step = 1;
const range range_r = {0,3};
const range range_K = {0,10};
const params_ranges_t params_ranges = { range_r, range_K };
const double p_step = .001;
const double p_rad = .0005;

/**************** SIMULATION CONFIG ***************/
const int nb_threads = 3;
const double delta = 0;
const double epsilon = .01;
const int outliers = 4;
const double error = .05;
const double precision = .05;

/**************** MISC *******************/
const bool create_random = true;
const int accuracy = 3;
#endif
