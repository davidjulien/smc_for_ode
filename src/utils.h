#ifndef H_UTILS
#define H_UTILS

#include <iostream>
#include <vector>
#include <random>
#include <boost/random.hpp>
#include <boost/array.hpp>
#include <algorithm>


template<const int S, const int P>
class Utils{
        typedef boost::array<double, S> state_t;
        typedef boost::array<double, P> params_t;
        typedef boost::array<double, 2> range; // min, max
        typedef std::vector<state_t> data_t;
        typedef boost::array<range, P> params_ranges_t;

        private:
                std::vector<double> m_data_time_points;
                std::vector<double> m_cmp_time_points;
                std::vector<int> m_time_flags;
                data_t m_exp_data;
                data_t m_exp_radiuses;
                params_ranges_t m_params_ranges;
                params_t m_params;
                double m_p_step;
                double m_p_rad;
                size_t m_index_var;

        public:
                /********* CONSTRUCTORS **********/
                Utils<S,P>()
                {}

                Utils<S,P>(const std::vector<double> data_time_points,
                                const std::vector<std::vector<double>> data,
                                const std::vector<std::vector<double>> data_radiuses,
                                const params_ranges_t params_ranges,
                                const double p_step, const double p_rad,
                                const double t_step, const double dt)
                        : m_data_time_points(data_time_points),
                        m_params_ranges(params_ranges), m_p_step(p_step),
                        m_p_rad(p_rad), m_index_var(1)
                {
                        create_exp_data(data);
                        create_exp_rad(data_radiuses);
                        expand_time_points(t_step, dt);
                        create_time_flags(dt);

                        m_params = params_t();
                        typename params_t::iterator it_params =
                                m_params.begin();
                        typename params_ranges_t::const_iterator it_ranges =
                                m_params_ranges.begin();
                        while (it_ranges != m_params_ranges.end()) {
                                (*it_params) = (*it_ranges)[0];
                                it_ranges++;
                                it_params++;
                        }
                }

                /********* METHODS **********/
                /*
                 * return       : current base parameters
                 */
                const params_t get_params()
                {
                        return m_params;
                }

                /*
                 * return       : the range of parameters to be tested
                 */
                const params_ranges_t get_params_ranges()
                {
                        return m_params_ranges;
                }

                /*
                 * return       : experimental data stored as data_t
                 */
                const data_t get_exp_data()
                {
                        return m_exp_data;
                }

                /*
                 * return       : experimental data time points
                 */
                const std::vector<double> get_data_time_points()
                {
                        return m_data_time_points;
                }

                /*
                 * return       : current experimental radiuses as data_t
                 */
                const data_t get_exp_radiuses()
                {
                        return m_exp_radiuses;
                }

                /*
                 * return       : computed time points
                 */
                const std::vector<double> get_cmp_time_points()
                {
                        return m_cmp_time_points;
                }

                /*
                 * return       : time flags (to compare cmp and exp data)
                 */
                const std::vector<int> get_time_flags()
                {
                        return m_time_flags;
                }

                /*
                 * state        : the state whose range is needed
                 * radius       : the corresponding radius
                 *
                 * return       : the range of its coordinate
                 */
                const std::vector<range> compute_range(const state_t state,
                                const state_t radius)
                {
                        std::vector<range> res;
                        res.reserve(S);
                        typename state_t::const_iterator it_state =
                                state.begin();
                        typename state_t::const_iterator it_rad =
                                radius.begin();

                        range tmp;

                        while(it_state != state.end()) {
                                tmp[0] = *it_state - *it_rad;
                                tmp[1] = *it_state + *it_rad;
                                res.push_back(tmp);
                                ++it_state;
                                ++it_rad;
                        }

                        return res;
                }

                /*
                 * normalize data values w.r.t. base
                 *
                 * data         : data to be normalized
                 * base         : normalization base
                */
                std::vector<double> normalize_data(
                                const std::vector<double> data,
                                const double base)
                {
                        std::vector<double> res = std::vector<double>();
                        res.reserve(data.size());
                        for (double value : data)
                                res.push_back(value / base);

                        return res;
                }

                /*
                 * start        : starting point
                 * end          : end point
                 * step         : difference between two time points
                 *
                 * return       : std::vector<double> of the time points
                 */
                std::vector<double> create_steps(const double start,
                                const double end, const double step)
                {
                        const size_t size = (end - start) / step + 1;
                        std::vector<double> res =
                                std::vector<double>(size, 0);
                        std::vector<double>::iterator it_res = res.begin();
                        for (double t = start; it_res != res.end(); t += step){
                                *it_res = t;
                                ++it_res;
                        }


                        return res;
                }

                /*
                 * data_time_points     : corresponding time points
                 * step         : minimal difference between two time points
                 * dt           : threshold to keep data time point
                 *
                 * return       : none
                 */
                void expand_time_points(
                                const double t_step, const double dt)
                {
                        const double start= m_data_time_points.front();
                        const double end= m_data_time_points.back();
                        std::vector<double>::const_iterator it_data =
                                m_data_time_points.begin();

                        std::vector<double> res =
                                create_steps(start, end, t_step);
                        std::vector<double>::iterator it_cmp =
                                res.begin();
                        while (it_cmp != res.end()) {
                                if (std::abs(*it_data - *it_cmp) < dt)
                                        *it_cmp = *it_data;
                                ++it_cmp;
                                while (it_data != m_data_time_points.end()
                                                and *it_data < *it_cmp)
                                        ++it_data;
                        }
                        m_cmp_time_points = res;
                }

                /*
                 * data_time_points     : experimental time points
                 * cmp_time_points      : computed time points
                 * dt           : threshold to keep data time point
                 *
                 * return       : a vector containing 0 everywhere except where
                 *              data timepoints are used
                 */
                void create_time_flags( const double dt)
                {
                        std::vector<int> res =
                                std::vector<int>(m_cmp_time_points.size(), 0);

                        std::vector<double>::const_iterator it_data =
                                m_data_time_points.begin();
                        std::vector<double>::const_iterator it_cmp =
                                m_cmp_time_points.begin();
                        std::vector<int>::iterator it_flags = res.begin();

                        while (it_cmp != m_cmp_time_points.end()
                                        and it_data != m_data_time_points.end()) {
                                if (std::abs(*it_data - *it_cmp) < dt)
                                        *it_flags = 1;
                                while (it_data != m_data_time_points.end()
                                                and *it_data < *it_cmp)
                                        ++it_data;
                                ++it_cmp;
                                ++it_flags;
                        }
                        m_time_flags = res;
                }

                /*
                 * data         : a vector of vectors of double, corresponding
                 *              to the data
                 */
                void create_exp_data(
                                const std::vector<std::vector<double>> data)
                {
                        std::vector<state_t> res = std::vector<state_t>();
                        res.reserve(m_data_time_points.size());

                        state_t state;

                        for (size_t t = 0; t < m_data_time_points.size(); t++) {
                                for (size_t i = 0; i < S; i++)
                                        state[i] = data[i][t];
                                res.push_back(state);
                        }
                        m_exp_data = res;
                }

                /*
                 * data_rad     : a vector of vectors of double, corresponding
                 *              to data radiuses
                 */
                void create_exp_rad(
                                const std::vector<std::vector<double>> data_rad)
                {
                        std::vector<state_t> res = std::vector<state_t>();
                        res.reserve(m_data_time_points.size());

                        state_t radius;

                        for (size_t t = 0; t < m_data_time_points.size(); t++) {
                                for (size_t i = 0; i < S; i++)
                                        radius[i] = data_rad[i][t];
                                res.push_back(radius);
                        }
                        m_exp_radiuses = res;
                }

                /*
                 * i            : index of the parameter
                 * value        : value to be fitted
                 *
                 * return       : the initial value, or the bound it exceeded
                 */
                double fit_in_range(const size_t i, const double value)
                {
                        double res = std::max(m_params_ranges[i][0],
                                        value);
                        res = std::min(res,
                                        m_params_ranges[i][1]);

                        return res;
                }

                /*
                 *
                 */
                bool variate_params()
                {
                        if (m_index_var <= P) {
                                bool inc = true;
                                for (size_t i = 0; i < m_index_var and inc; i++) {
                                        m_params[i] += m_p_step;
                                        double max = m_params_ranges[i][1]
                                                + (m_p_step / 2);
                                        if (m_params[i] >= max) {
                                                m_params[i] = m_params_ranges[i][0];
                                                if (i == m_index_var - 1) {
                                                        if (m_index_var < P)
                                                                m_params[i+1] +=
                                                                        m_p_step;
                                                        ++m_index_var;
                                                        break;
                                                }
                                        } else
                                                inc = false;
                                }
                        }
                        return m_index_var > P;
                }

                /*
                 * return       : a set of paramaters picked in the ranges
                 */
                params_t generate_params()
                {
                        std::random_device rd;
                        params_t res;
                        for (size_t i = 0; i < P; i++) {
                                boost::random::mt19937 rng(rd());
                                boost::random::uniform_real_distribution<float>
                                        distrib(-m_p_rad, m_p_rad);
                                double tmp = m_params[i] + distrib(rng);
                                res[i] = fit_in_range(i, tmp);

                        }

                        return res;
                }

                /*
                 * return       : a set of paramaters picked in the ranges
                 */
                params_t generate_toy_params()
                {
                        std::random_device rd;
                        params_t res;
                        for (size_t i = 0; i < P; i++) {
                                boost::random::mt19937 rng(rd());
                                boost::random::uniform_real_distribution<float>
                                        distrib(m_params_ranges[i][0],
                                                        m_params_ranges[i][1]);
                                res[i] = m_params[i] + distrib(rng);
                        }

                        return res;
                }

};

#endif
