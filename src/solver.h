/*
 * @author      : David JULIEN (based on Gilles ARDOUREL)
 * @date        : Friday, March 25th
 * @license     : GPLv3
 *
 *
 */

#ifndef H_SOLVER
#define H_SOLVER

#include <iostream>
#include <vector>
#include <string>
#include <cmath>
#include <boost/array.hpp>
#include "ode.h"


template<const int S, const int P>
class Solver{
        typedef boost::array<double, S> state_t;
        typedef boost::array<double, P> params_t;

        private:
                ODE<S,P> m_ode;
                std::vector<double> m_time_points;
                std::vector<double>::iterator m_current_time;
                std::vector<int> m_time_flags;

        public:
                /* CONSTRUCTORS */
                Solver(const params_t params, const state_t state_0,
                                const std::vector<double> time_points)
                        : m_ode(ODE<S,P>(state_0, params))
                {
                        m_time_points = time_points;
                        m_current_time = m_time_points.begin();
                }

                /* METHODS */

                /*
                 * return       : current state
                 */
                state_t get_state()
                {
                        return m_ode.get_state();
                }

                /*
                 * return       : current time
                 */
                double get_time()
                {
                        return *m_current_time;
                }

                /*
                 * return       : true if the end of time_points is reached
                 *                false otherwise
                 */
                bool can_progress()
                {
                        return m_current_time != m_time_points.end();
                }

                /******* RESOLUTION METHODS *******/

                /*
                 * t0           : start of the integration
                 * t1           : end of the integration
                 *
                 * return       : none (update ode state)
                 */
                void runge_kutta_4(const double t0,
                                const double t1)
                {
                        const double dt = std::abs(t0 - t1);
                        state_t old = m_ode.get_state();
                        state_t res = old;
                        state_t k1, k2, k3, k4;
                        state_t x_k1, x_k2, x_k3;

                        k1 = m_ode.compute_next_state();

                        for (int j = 0; j < S; ++j)
                        {
                                x_k1[j] = old[j] + k1[j] * dt / 2;
                        }
                        m_ode.update(x_k1);

                        k2 = m_ode.compute_next_state();

                        for (int j = 0; j < S; ++j)
                        {
                                x_k2[j] = old[j] + k2[j] * dt / 2;
                        }
                        m_ode.update(x_k2);

                        k3 = m_ode.compute_next_state();

                        for (int j = 0; j < S; ++j)
                        {
                                x_k3[j] = old[j] + k3[j] * dt;
                        }
                        m_ode.update(x_k3);

                        k4 = m_ode.compute_next_state();

                        for (int j = 0; j < S; ++j)
                        {
                                res[j] += dt * (k1[j] + k2[j] * 2 + k3[j] * 2
                                                + k4[j]) / 6;
                        }

                        m_ode.update(res);
                }

                void progress_RK4()
                {
                        const double t0 = *m_current_time;
                        const double t1 = *(++m_current_time);
                        runge_kutta_4(t0, t1);
                }
};

#endif
