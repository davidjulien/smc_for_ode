/*
 * @author      : David JULIEN (based on Gilles ARDOUREL)
 * @date        : Friday, March 25th
 * @license     : GPLv3
 *
 *
 */

#ifndef H_ODE
#define H_ODE

#include <iostream>
#include <vector>
#include <string>
#include <boost/array.hpp>


template<const int S, const int P>
class ODE{
        typedef boost::array<double, S> state_t;
        typedef boost::array<double, P> params_t;

        private:
                state_t m_state;
                params_t m_params;

        public:
                /* CONSTRUCTORS */
                ODE<S,P>()
                        : m_state(zero_s()),
                          m_params(zero_p())
                {}

                ODE<S,P>(const state_t state_0, const params_t params_0)
                        : m_params(params_0)
                {
                        update(state_0);
                }

                /* METHODS */

                /*
                 * zero-population state_t
                 *
                 * return       : {0, 0, ...} state_t
                 */
                constexpr state_t zero_s()
                {
                        return boost::array<double, S>();
                }

                /*
                 * zero params_t
                 *
                 * return       : {0, 0, ...} params_t
                 */
                constexpr params_t zero_p()
                {
                        return boost::array<double, P>();
                }

                /*
                 * return       : current state
                 */
                state_t get_state()
                {
                        return m_state;
                }

                /*
                 * return       : ODE parameters
                 */
                params_t get_params()
                {
                        return m_params;
                }

                /*
                 * new_s        : new state of the approximation
                 *
                 * return       : none
                 */
                void update(const state_t new_s)
                {
                        m_state = new_s;
                }

                /*
                 * computes the next step value in the model
                 *
                 * old          : old state_t
                 * params       : parameters to be tested
                 *
                 * return       : new state_t
                 */
                state_t compute_next_state()
                {
                        double x_old = m_state[0];
                        double r = m_params[0];
                        double K = m_params[1];
                        double x_new = r * x_old * (1 - (x_old / K));

                        update({x_new});

                        return m_state;
                }
};

#endif
