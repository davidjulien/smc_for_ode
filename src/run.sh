#!/usr/bin/env sh

root_dir="../"
dir="$root_dir/res_"

make clean; make || exit 1
mkdir "$dir" && cd "./$dir"
printf "Build complete, running simulations..."
/usr/bin/time -o time.txt $root_dir/src/main

printf " Done.\nTrimming results..."
header(){ printf "params,min,max,distance\n"; }
header > score_0.csv
header > score_non0.csv
header > score_1.csv

format() { sed -e 's/.*(//' -e 's/),min=/,/' -e 's/max=//' -e 's/avg=//' -e 's/dist=//' -e 's/;/,/g' -e 's/ //g'; }
grep "avg=0," res_*.csv | format >> score_0.csv
grep "avg=0\." res_*.csv | format >> score_non0.csv
grep "avg=1" res_*.csv | format >> score_1.csv

printf " Done.\n"
