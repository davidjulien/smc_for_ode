/*
 * @author      : David JULIEN (based on Gilles ARDOUREL)
 * @date        : Thursday, March 24th
 * @license     : GPLv3
 *
 *
 */

#include <iostream>
#include <fstream>
#include <iomanip>
#include <string>
#include <float.h>
#include <vector>
#include <algorithm>
#include <numeric>
#include <cmath>
#include <cstdlib>
#include <thread>
#include <future>
#include "solver.h"
#include "utils.h"
#include "config.h"


std::ostream &operator<<(std::ostream &os, const state_t &v)
{
        for (size_t i = 0; i < v.size(); ++i)
        {
                os << v[i];
                if (i != v.size() - 1)
			os << ",";
        }

        return os;
}

std::ostream &operator<<(std::ostream &os, const params_t &v)
{
        os << "(";
        for (size_t i = 0; i < v.size(); ++i)
        {
                os << v[i];
                if (i != v.size() - 1)
                        os << ",";
        }
        os << ")";

        return os;
}

std::ostream &operator<<(std::ostream &os, const params_ranges_t &v)
{
        for (size_t i = 0; i < v.size(); i++) {
                os << "[" << v[i][0] << "," << v[i][1] << "]";
                if (i != v.size() - 1)
                        os << " x ";
        }

        return os;
}

/*
 * ranges               : the ranges to be divided
 * n                    : the number of divisions
 *
 * return               : a vector of params_ranges_t with sub-ranges
 */
std::vector<params_ranges_t> divide_params_ranges(
                const params_ranges_t ranges, const size_t n)
{
        std::vector<params_ranges_t> res;
        res.reserve(n);

        for (size_t i = 0; i < n; i++) {
                params_ranges_t tmp;
                double range = ranges[0][1] - ranges[0][0];
                double step = range / n;
                double min = ranges[0][0] + i * step;
                double max;
                if (i == (n - 1))
                        max = ranges[0][1];
                else
                        max = ranges[0][0] + (i + 1) * step;
                tmp[0] = {min, max};
                for (size_t j = 1; j < params_size; j++)
                        tmp[j] = {ranges[j][0], ranges[j][1]};
                res.push_back(tmp);
        }

        return res;
}

/*
 * value                :state_t
 * *
 * return               : true if one of the value is inf
 */
bool isinf(const state_t value)
{
	        bool res = false;
                for (size_t i = 0; i < value.size() and !res; i++)
                        res = std::isinf(value[i]);

                return res;
}

/*
 * tunnel               : the tunnel of data to compare computations
 * state                : the state to verify
 * delta                : distance from the sides
 *
 * return               : true if the state is within the polygon,
 *                        false otherwiser
 */
bool is_in_tunnel(const std::vector<range> tunnel, const state_t state,
                const double delta)
{
        bool res = true;
        for (size_t i = 0; i < state_size and res; i++) {
                res = tunnel[i][0] - delta <= state[i]
                        and tunnel[i][1] + delta >= state[i];
        }

        return res;
}

/*
 * origin               : state_t
 * value                : state_t
 *
 * return               : euclidian distance between origin and value
 */
double euclidian_distance(const state_t origin, const state_t value)
{
        double tmp = 0;
        for (size_t i = 0; i < state_size; i++) {
                if (std::isinf(value[i])) {
			tmp = std::numeric_limits<double>::infinity();
                        break;
		} else
                        tmp += std::pow(value[i] - origin[i], 2);
        }

        return std::sqrt(tmp);
}

/*
 * origin               : state_t
 * value                : state_t
 *
 * return               : euclidian distance between origin and value
 */
double infinity_distance(const state_t origin, const state_t value)
{
        double res = 0;
        for (size_t i = 0; i < state_size; i++) {
                if (std::isinf(value[i])) {
			res = std::numeric_limits<double>::infinity();
                        break;
		} else
                        res = std::max(res, std::abs(value[i] - origin[i]));
        }

        return res;
}

/*
 * utils                : utilitaries helper
 * exp_data             : experimental data
 * cmp_data             : computed data
 * time_flags           : vector of flags for matching data
 * exp_radiuses         : radiuses of data_points
 *
 * return               : a tuple containing the sum of distance from the data
 *                          and the number of steps out of tunnel 1 and 2
 */
std::tuple<double, int> check_simulation(Utils<state_size,
                params_size> utils, std::vector<state_t> cmp_data,
                double delta)
{
        std::vector<state_t> exp_data = utils.get_exp_data();
        std::vector<int> time_flags = utils.get_time_flags();
        std::vector<state_t> exp_radiuses = utils.get_exp_radiuses();
        std::vector<state_t>::iterator it_data =
                exp_data.begin();
        std::vector<state_t>::iterator it_rad =
                exp_radiuses.begin();
        std::vector<state_t>::iterator it_cmp =
                cmp_data.begin();
        std::vector<int>::iterator it_flags = time_flags.begin();
        double distance = 0;
        int strike = 0;

        while (it_cmp != cmp_data.end()
                        and it_data != exp_data.end()) {
                if (*it_flags == 1) {
                        std::vector<range> tunnel = utils.compute_range(
                                        *it_data, *it_rad);
                        if (!is_in_tunnel(tunnel, *it_cmp, delta))
                                strike++;
                        distance = std::max(distance,
                                infinity_distance(*it_data, *it_cmp));
                        ++it_data;
                        ++it_rad;
                }
                ++it_cmp;
                ++it_flags;
        }

        return std::make_tuple(distance, strike);
}

/*
 * params       : set of parameters to be tested
 *
 * return       : none
 */
std::vector<state_t> simulate(Utils<state_size, params_size> utils,
                params_t params)
{
        std::vector<double> time_points = utils.get_cmp_time_points();
        state_t state_0 = utils.get_exp_data()[0];

        Solver<state_size, params_size> solver(params, state_0, time_points);
        std::vector<state_t> res;
        res.reserve(time_points.size());

        while (solver.can_progress()) {
                state_t current_state = solver.get_state();
                res.push_back(current_state);
                solver.progress_RK4();
        }

        return res;
}

/*
 * thread_id            : id of the thread
 * params_ranges        : ranges to be checked
 *
 * return               : a tuple containing:
 *                              the best parameter
 *                              the distance between the induced solution
 *                                      and the data
 *                              the score
 */
std::array<simres_t,2> work(const int thread_id,
                const params_ranges_t params_ranges,
                const int nb_sim)
{
        std::ofstream output;
        output.open("res_" + std::to_string(thread_id) + ".csv");
        output << "**********    thread_id: " << thread_id << std::endl;
        Utils<state_size, params_size> utils(data_time_points, data,
                        data_radiuses, params_ranges, p_step, p_rad,
                        t_step, dt);
        output << "********** ranges to be tested: "
                << utils.get_params_ranges() << "\n\n" << std::endl;

        bool stop = false;
        simres_t best_s;
        simres_t best_d;
        std::get<1>(best_s) = 0;
        std::get<1>(best_d) = 0;
        std::get<2>(best_s) = FLT_MAX;
        std::get<2>(best_d) = FLT_MAX;
        while (!stop) {
                double sim_delta;
                int valid[3] = {0, 0, 0};
                double dist[3] = {0, 0, 0};
                for (int i = 1; i < 3; i++) {
                        if (i == 1)
                                sim_delta = 0 - epsilon;
                        else
                                sim_delta = delta + epsilon;
                        for (int j = 0; j < nb_sim; j++){
                                params_t params = utils.generate_params();
                                std::vector<state_t> cmp_data =
                                        simulate(utils, params);
                                std::tuple<double, int> res =
                                        check_simulation(utils, cmp_data,
                                                sim_delta);
                                dist[i] += std::get<0>(res)/nb_sim;
                                if (std::get<1>(res) < outliers)
                                        (valid[i])++;
                        }
                }

                float score1 = ((float) valid[1]) / nb_sim;
                float score2 = ((float) valid[2]) / nb_sim;
                double score = (score1 + score2) / 2;
                double dist_max = std::max(dist[1], dist[2]);
		if (score1 > std::get<1>(best_s)){
			std::get<0>(best_s) = utils.get_params();
			std::get<1>(best_s) = score;
			std::get<2>(best_s) = dist_max;
		}
                if (dist_max < std::get<2>(best_d)){
			std::get<0>(best_d) = utils.get_params();
			std::get<1>(best_d) = score;
			std::get<2>(best_d) = dist_max;
		}

                output << "params=" << utils.get_params() << ","
                        << "min=" << score1 << ","
                        << "max=" << score2 << ","
                        << "avg=" << score << ","
                        << "dist=" << dist_max << std::endl;
                stop = utils.variate_params();
        }

	return {best_s,best_d};
}

int main(int argc, char* argv[])
{
        const long nb_sim = 50;
        const double theta = 1 - std::sqrt(1 - precision);
        //long nb_sim = ceil(std::log(2. / theta) / (2 * std::pow(error,2)));

        std::ofstream output;
        output.open("config.txt");
        output << "***** ODE CONFIG *****" << std::endl;
        output << "t_step : " << t_step << std::endl;
        output << "params : " << params_ranges << std::endl;
        output << "p_step : " << p_step << std::endl;
        output << "p_rad : " << p_rad << std::endl;
        output << "\n***** SIMULATION CONFIG *****" << std::endl;
        output << "error : " << error << std::endl;
        output << "precision : " << precision << std::endl;
        output << "nb_sim : " << nb_sim << std::endl;
        output << "epsilon : " << epsilon << std::endl;
        output << "outliers_max : " << outliers << std::endl;
	output.close();

        std::setprecision(accuracy);

        for (size_t i = 0; i < data.size(); i++){
                output.open("data_" + std::to_string(i) + ".csv");
                output << "t,min,data,max" << std::endl;
                for (size_t j = 0; j < data_time_points.size(); j++) {
                        double tmp_d = data[i][j];
                        double tmp_r = data_radiuses[i][j];
                        output << data_time_points[j] << "," << tmp_d - tmp_r
                        << "," << tmp_d << "," << tmp_d + tmp_r << std::endl;
                }
                output.close();
        }

        std::future<std::array<simres_t,2>> threads[nb_threads];
        std::array<simres_t,2> res[nb_threads];
        std::vector<params_ranges_t> ranges = divide_params_ranges(
                        params_ranges, nb_threads);

        for (int i = 0; i < nb_threads; i++)
                threads[i] = std::async(&work, i, ranges[i], nb_sim);
        for (int i = 0; i < nb_threads; i++)
                res[i] = threads[i].get();

        Utils<state_size, params_size> utils(data_time_points, data,
                        data_radiuses, params_ranges, p_step, p_rad,
                        t_step, dt);
        std::vector<double> cmp_time_points = utils.get_cmp_time_points();
               cmp_time_points.begin();
        std::vector<std::string> rewards = { "score", "dist" };
        for (size_t j = 0; j < rewards.size() ; j++){
                std::string rew = rewards[j];
                output.open("bests_" + rew +".txt");
                for (int i = 0; i < nb_threads; i++) {
                        output << "thread_" << i << " : "
                                << std::get<0>(res[i][j])
                                << " *** score : "
                                << std::get<1>(res[i][j])
                                << " *** distance : "
                                << std::get<2>(res[i][j])
                                << std::endl;
                }
                output << std::endl;

                simres_t best = res[0][j];
                if ("score" == rew) {
                        for (int i = 0; i < nb_threads; i++) {
                                if (std::get<1>(res[i][j]) > std::get<1>(best))
                                        best = res[i][j];
                        }
                } else {
                        if ("dist" == rew)
                                for (int i = 0; i < nb_threads; i++) {
                                        if (std::get<2>(res[i][j]) < std::get<2>(best))
                                                best = res[i][j];
                                }
                }

                std::vector<state_t> cmp_data =
                        simulate(utils, std::get<0>(best));
                std::vector<state_t>::iterator it_cmp = cmp_data.begin();

                output << "*** best parameter : " << std::get<0>(best)
                        << " *** score = " << std::get<1>(best)
                        << " *** distance = " << std::get<2>(best) << std::endl;
                output.close();

                output.open("cmp_best_" + rew + ".csv");
                output << "t,state" << std::endl;
                std::vector<double>::const_iterator it_cmp_time =
                               cmp_time_points.begin();
                while (it_cmp != cmp_data.end()){
                        output << *it_cmp_time << "," << *it_cmp << std::endl;
                        ++it_cmp;
                        ++it_cmp_time;
                }
                output.close();
        }

        if (create_random) {
                for (int i = 0; i < 8; i++) {
                        params_t params = utils.generate_toy_params();
                        std::vector<double>::const_iterator it_cmp_time =
                               cmp_time_points.begin();
                        std::vector<state_t> cmp_data =
                                simulate(utils, params);
                        std::vector<state_t>::iterator it_cmp =
                                cmp_data.begin();

                        output.open("rand_params.txt");
                        output << "*** parameter : " << params << std::endl;
                        output.close();
                        output.open("rand_" + std::to_string(i) + ".csv");
                        output << "t,state" << std::endl;
                        while (it_cmp != cmp_data.end()){
                                output << *it_cmp_time << "," << *it_cmp
                                        << std::endl;
                                ++it_cmp;
                                ++it_cmp_time;
                        }
                        output.close();
                }
        }

        return 0;
}
